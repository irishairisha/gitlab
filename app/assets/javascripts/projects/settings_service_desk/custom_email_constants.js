import { s__, __ } from '~/locale';

export const FEEDBACK_ISSUE_URL = 'https://gitlab.com/gitlab-org/gitlab/-/issues/416637';

export const I18N_LOADING_LABEL = __('Loading');
export const I18N_CARD_TITLE = s__('ServiceDesk|Configure a custom email address');
export const I18N_FEEDBACK_PARAGRAPH = s__(
  'ServiceDesk|Please share your feedback on this feature in the %{linkStart}feedback issue%{linkEnd}',
);
export const I18N_GENERIC_ERROR = __('An error occurred. Please try again.');
